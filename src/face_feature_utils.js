import mpFaceMesh from '@mediapipe/face_mesh';

const RIGHT_EYE_VERTS = [33, 7, 163, 144, 145, 153, 154, 155, 133, 173, 157, 158, 159, 160, 161, 246, 33];

const RIGHT_EYE_CORNERS = [33,133];
// var RIGHT_EYE_TOP_BOTTOM = [ 159, 145 ];
const RIGHT_EYE_TOP_BOTTOM = [ 27, 23 ];

const LEFT_EYE_VERTS = [263, 249, 390, 373, 374, 380, 381, 382, 362, 398, 384, 385, 387, 388, 466, 263];

const LEFT_EYE_CORNERS = [263,362];
// var LEFT_EYE_TOP_BOTTOM = [ 386, 374 ];
const LEFT_EYE_TOP_BOTTOM = [ 257, 253 ];

const NOSE_PROFILE_VERTS = [2, 194, 19, 1, 4, 5, 195] + [141,125,44,45,51] + [270,354,274,275,261];
const NOSE_TIP = 1;

const CHIN = 152;

const NOSE_EYE_RATIO_OFFSET = 0.1;
const NOSE_EYE_CHIN_RATIO_OFFSET = -0.15;
const IRIS_EYE_RATIO_OFFSET = -1.0;

// const EYE_CLOSED_VALUE = 0.00018 * 1000.0;
const EYE_CLOSED_VALUE = 0.000018 * 1000.0;
// const EYE_CLOSED_VALUE = 0.020;


function landmarks_to_array(face_landmarks) {
    return face_landmarks.map(lm => [ lm.x, lm.y, lm.z ]);
}

function iris_look_direction(lm) {
    const LEFT_IRIS_VERTS = mpFaceMesh.FACEMESH_LEFT_IRIS.map(s => s[0]);
    const RIGHT_IRIS_VERTS = mpFaceMesh.FACEMESH_RIGHT_IRIS.map(s => s[0]);

    let left_iris_x = LEFT_IRIS_VERTS.map(i => lm[i][0]);
    let left_sum_x = left_iris_x.reduce((sum, num) => sum + num, 0);
    let left_x = left_sum_x / LEFT_IRIS_VERTS.length;
    let left_iris_left_offset_x = Math.abs(lm[LEFT_EYE_CORNERS[0]][0] - left_x);
    let left_iris_right_offset_x = Math.abs(lm[LEFT_EYE_CORNERS[1]][0] - left_x);
    let left_ratio_x = Math.log(left_iris_right_offset_x / left_iris_left_offset_x);

    let right_iris_x = RIGHT_IRIS_VERTS.map(i => lm[i][0]);
    let right_sum_x = right_iris_x.reduce((sum, num) => sum + num, 0);
    let right_x = right_sum_x / RIGHT_IRIS_VERTS.length;
    let right_iris_left_offset_x = Math.abs(lm[RIGHT_EYE_CORNERS[0]][0] - right_x);
    let right_iris_right_offset_x = Math.abs(lm[RIGHT_EYE_CORNERS[1]][0] - right_x);
    let right_ratio_x = Math.log(right_iris_left_offset_x / right_iris_right_offset_x);

    let x = (left_ratio_x + right_ratio_x) / 2.0;

    let v_left_eye_bottom = lm[LEFT_EYE_TOP_BOTTOM[0]][1];
    let v_left_eye_top = lm[LEFT_EYE_TOP_BOTTOM[1]][1];
    let left_iris_y = LEFT_IRIS_VERTS.map(i => lm[i][1]);
    let left_sum_y = left_iris_y.reduce((sum, num) => sum + num, 0);
    let left_y = left_sum_y / LEFT_IRIS_VERTS.length;

    let v_right_eye_bottom = lm[RIGHT_EYE_TOP_BOTTOM[0]][1];
    let v_right_eye_top = lm[RIGHT_EYE_TOP_BOTTOM[1]][1];

    let right_iris_y = RIGHT_IRIS_VERTS.map(i => lm[i][1]);
    let right_sum_y = right_iris_y.reduce((sum, num) => sum + num, 0);
    let right_y = right_sum_y / RIGHT_IRIS_VERTS.length;

    let left_ratio_y = ((left_y > v_left_eye_top) || (left_y < v_left_eye_bottom)) ? 0 :
        (Math.abs(1.52*(v_left_eye_top - left_y)) / Math.abs((left_y - v_left_eye_bottom))) + IRIS_EYE_RATIO_OFFSET;
    let right_ratio_y = ((right_y > v_right_eye_top) || (right_y < v_right_eye_bottom)) ? 0 :
        (Math.abs(1.52*(v_right_eye_top - right_y)) / Math.abs((right_y - v_right_eye_bottom))) + IRIS_EYE_RATIO_OFFSET;

    let y = (left_ratio_y + right_ratio_y) / 2.0;

    return [x,y];
}

function eye_nose_ratio(lm) {
    var left_eye_outer_x = lm[LEFT_EYE_CORNERS[0]][0];
    var left_eye_inner_x = lm[LEFT_EYE_CORNERS[1]][0];
    var right_eye_outer_x = lm[RIGHT_EYE_CORNERS[0]][0];
    var right_eye_inner_x = lm[RIGHT_EYE_CORNERS[1]][0];
    var nose_x = lm[NOSE_TIP][0];
    return Math.log((Math.abs(nose_x - left_eye_outer_x)) / Math.abs((nose_x - right_eye_outer_x)))+NOSE_EYE_RATIO_OFFSET;
}

function eye_nose_chin_ratio(lm) {
    var left_eye_outer_y = lm[LEFT_EYE_CORNERS[0]][1];
    var right_eye_outer_y = lm[RIGHT_EYE_CORNERS[0]][1];
    var eye_y = (left_eye_outer_y + right_eye_outer_y) / 2.0;
    var chin_y = lm[CHIN][1];
    var nose_y = lm[NOSE_TIP][1];
    // distance nose to eye is about half of nose to chin, scale accordingly
    return Math.log((Math.abs(nose_y - eye_y)*2.2) /
                    Math.abs((nose_y - chin_y))) + NOSE_EYE_CHIN_RATIO_OFFSET;
}


const dot = (a, b) => a.map((x, i) => a[i] * b[i]).reduce((m, n) => m + n);

function roll(x) {
    var xx = Array.from(x);
    xx.push(xx.shift());
    return xx;
}

function poly_area(x,y) {
    // console.log(x,y,dot(x,roll(y)), dot(y,roll(x)) ,  Math.abs(dot(x,roll(y)) - dot(y,roll(x))) )
    return 0.5 * Math.abs(dot(x,roll(y)) - dot(y,roll(x)));
}
// Note: to detect double blink: should both eyes be closed? or is one closed eye enough?
function is_blinking(lm) {

    let left_eye_x = LEFT_EYE_VERTS.map(i => lm[i][0]);
    let left_eye_y = LEFT_EYE_VERTS.map(i => lm[i][1]);
    var left_eye_area = poly_area(left_eye_x, left_eye_y) * 1000.0;

    let right_eye_x = RIGHT_EYE_VERTS.map(i => lm[i][0]);
    let right_eye_y = RIGHT_EYE_VERTS.map(i => lm[i][1]);
    var right_eye_area = poly_area(right_eye_x, right_eye_y) * 1000.0;
    // console.log(left_eye_area, right_eye_area);
    return left_eye_area < EYE_CLOSED_VALUE || right_eye_area < EYE_CLOSED_VALUE;
}

export {
    landmarks_to_array,
    eye_nose_ratio,
    eye_nose_chin_ratio,
    iris_look_direction,
    is_blinking
};
