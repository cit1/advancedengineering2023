



function set_active_quadrant(active_quad, fullscreen) {
    let small = "scale(0.5)";
    let normal = "scale(1.5)";
    let big = fullscreen ? "scale(4.0)" : "scale(2.5)";

//    active_quad = 0;
    if (active_quad ==0) {
        document.getElementById("test1").style.transform = normal;
        document.getElementById("test2").style.transform = normal;
        document.getElementById("test3").style.transform = normal;
        document.getElementById("test4").style.transform = normal;

        document.getElementById("test1").style.zIndex = "4";
        document.getElementById("test2").style.zIndex = "4";
        document.getElementById("test3").style.zIndex = "4";
        document.getElementById("test4").style.zIndex = "4";
      }

    if (active_quad ==1 ) {
        document.getElementById("test1").style.transform = big;
        document.getElementById("test2").style.transform = small;
        document.getElementById("test3").style.transform = small;
        document.getElementById("test4").style.transform = small;

        document.getElementById("test1").style.zIndex = "1";
        document.getElementById("test2").style.zIndex = "4";
        document.getElementById("test3").style.zIndex = "4";
        document.getElementById("test4").style.zIndex = "4";
    }
    if (active_quad ==2 ) {
        document.getElementById("test1").style.transform = small;
        document.getElementById("test2").style.transform = big;
        document.getElementById("test3").style.transform = small;
        document.getElementById("test4").style.transform = small;

        document.getElementById("test1").style.zIndex = "4";
        document.getElementById("test2").style.zIndex = "1";
        document.getElementById("test3").style.zIndex = "4";
        document.getElementById("test4").style.zIndex = "4";
    }
    if (active_quad ==3 ) {
        document.getElementById("test1").style.transform = small;
        document.getElementById("test2").style.transform = small;
        document.getElementById("test3").style.transform = big;
        document.getElementById("test4").style.transform = small;

        document.getElementById("test1").style.zIndex = "4";
        document.getElementById("test2").style.zIndex = "4";
        document.getElementById("test3").style.zIndex = "1";
        document.getElementById("test4").style.zIndex = "4";
    }
    if (active_quad ==4 ) {
        document.getElementById("test1").style.transform = small;
        document.getElementById("test2").style.transform = small;
        document.getElementById("test3").style.transform = small;
        document.getElementById("test4").style.transform = big;

        document.getElementById("test1").style.zIndex = "4";
        document.getElementById("test2").style.zIndex = "4";
        document.getElementById("test3").style.zIndex = "4";
        document.getElementById("test4").style.zIndex = "1";
    }
 }

 function get_time_diff (start_time) {
    var now = new Date().getTime();
    var distance = now - start_time;
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    return minutes * 60 + seconds
}

   const allEqual = arr => arr.every( v => v === arr[0] )



   function median(numbers) {
      const sorted = Array.from(numbers).sort((a, b) => a - b);
      const middle = Math.floor(sorted.length / 2);

      if (sorted.length % 2 === 0) {
          return (sorted[middle - 1] + sorted[middle]) / 2;
      }

      return sorted[middle];
  }

  function to_percent(val) {
      const v = val * 100
      var res = 0
      if (v>97) { res = 97.5}
      else if (v<4) { res = 3.5}
      else { res = v}

      return res
  }


 export {
    set_active_quadrant,
    to_percent,
    median,
    allEqual,
    get_time_diff
 };
