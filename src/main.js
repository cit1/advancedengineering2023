import '@fortawesome/fontawesome-free/css/all.css';
import { createApp } from 'vue';
import App from './App.vue';
import {ref} from 'vue';


import VuePlyr from 'vue-plyr';
import 'vue-plyr/dist/vue-plyr.css';

// Vuetify
import 'vuetify/styles';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import { aliases, mdi } from 'vuetify/iconsets/mdi';

import mpFaceMesh from '@mediapipe/face_mesh';
import drawingUtils from '@mediapipe/drawing_utils';
import CircularBuffer from 'circular-buffer';

const vuetify = createVuetify({
  components,
  directives
});

const app = createApp(App).use(vuetify).use(VuePlyr, {plyr: {} }).mount('#app');

 /**
 * Below is the mediapipe facemesh example converted to plain js
 *
 */

import KalmanFilter from 'kalmanjs';

const kfx = new KalmanFilter({R: 0.01, Q: 3});
const kfy = new KalmanFilter({R: 0.01, Q: 5});


const face_feature_utils = require('./face_feature_utils.js');
const utils = require('./utils.js');

var iris_buffer = new CircularBuffer(10);

const solutionOptions = {
    selfieMode: true,
    enableFaceGeometry: false,
    maxNumFaces: 1,
    refineLandmarks: true,
    minDetectionConfidence: 0.7,
    minTrackingConfidence: 0.5
};

 const config = {
     locateFile: (file) => {
         return `https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh@` +
             `${mpFaceMesh.VERSION}/${file}`;
     }
 };

 function get_active_quad(sx, sy) {
     const sep_x = 0.1;
     const sep_y = 0.1;
     if(sx < (0.5 - sep_x) && sy < (0.5 - sep_y)) {
         return 1;
     }
     if(sx > (0.5 + sep_x) && sy < (0.5 - sep_y)) {
         return 2;
     }
     if(sx < (0.5 - sep_x) && sy > (0.5 + sep_y)) {
         return 3;
     }
     if(sx > (0.5 + sep_x) && sy > (0.5 + sep_y)) {
         return 4;
     }
     return 0;
 }

 const quadbuffer_size = 30;
var quadrant_buffer = new CircularBuffer(quadbuffer_size);

const blink_buffer_size = 10;
var blink_buffer = new CircularBuffer(blink_buffer_size);

// If we double blink we lock the current quadrant for a while

const selection_timeout = 45; // unselects the selection in seconds
const selection_timein = 5; // selects quadrant if stared at for the given seconds

var entered_selection = false;
var start_time_out = null; //new Date().getTime();
var prev_quad = 0;

var start_time_in = null

function detect_double_blink(blinkBuffer) {
    let blinks = 0;
    let isBlinking = false;

    for (let i = 0; i < blinkBuffer.length; i++) {
        let b = blinkBuffer[i];

        if (b) {
            if (!isBlinking) {
                blinks += 1;
                isBlinking = true;

                if (blinks >= 2) {
                    return true;
                }
            }
        } else {
            isBlinking = false;
        }
    }

    return blinks >= 2;
}

function move_selector_box(sx, sy) {
    document.getElementById("quad-selector").style.left =  `${utils.to_percent(kfx.filter(sx,1))}%`
    document.getElementById("quad-selector").style.top =  `${utils.to_percent(kfy.filter(sy,1))}%`
}

function reset_selector_box() {

    document.getElementById("canvas-out").style.display = "none"
    document.getElementById("quad-selector").style.left = "50%"
    document.getElementById("quad-selector").style.top = "50%"
    document.getElementById("quad-selector").hidden = true

}

function unhide_selector_box() {
    document.getElementById("canvas-out").style.display = "block"
    document.getElementById("quad-selector").hidden = false
    //
}


function most_frequent(arr, perc) {
    const count = {};
    for (const val of arr) {
        count[val] = (count[val] || 0) + 1;
    }
    const mostFrequent = Object.keys(count).reduce((a, b) => count[a] > count[b] ? a : b);
    const percent = count[mostFrequent] / arr.length;
    return percent >= perc ? parseInt(mostFrequent) : 0;
}



var locked_quad = 0;
function quadrant_selector(active_quad, double_blink) {
    quadrant_buffer.enq(active_quad);
    var squad = most_frequent(quadrant_buffer.toarray(), 0.7);

    let start_lock = !entered_selection && double_blink;
    let end_lock = entered_selection && double_blink;

    // selection auto timeout
    if (start_time_out != null) {
        let diff = utils.get_time_diff(start_time_out);
        if (diff>selection_timeout){
            end_lock =true;
            console.log("selection timed out !!!")
        }
    }

    // selection auto timein
    if (squad != 0 && squad == prev_quad && !entered_selection) {
        if (start_time_in == null) {
            start_time_in = new Date().getTime();
        } else {
            let diff = utils.get_time_diff(start_time_in);
            if (diff> selection_timein) {
                start_lock = true;
                console.log("selection timed in !!!")
            }
        }

    } else {
        start_time_in = null
    }

    prev_quad = Number(squad)

    // if(start_lock && squad == 0) {
    //     console.log("Double blink but not starting...: " + quadrant_buffer.toarray());
    // }
    if(start_lock && squad != 0) {
        locked_quad = squad;
        entered_selection = true;
        start_time_out = new Date().getTime();
        console.log("blinked!!")
        reset_selector_box();
    }
    if(end_lock && locked_quad != 0) {
        entered_selection = false;
        start_time_out = null;
        locked_quad = 0;
        unhide_selector_box();
    }



    if(locked_quad != 0) {
        utils.set_active_quadrant(locked_quad, true);
    } else {
        utils.set_active_quadrant(active_quad, false);
    }
}

 function onResults(results) {
     canvasCtx.save();
     canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
     canvasCtx.drawImage(
         results.image, 0, 0, canvasElement.width, canvasElement.height);

     if (results.multiFaceLandmarks) {
         for (const landmarks of results.multiFaceLandmarks) {
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_TESSELATION,
                 {color: '#C0C0C070', lineWidth: 1});
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_RIGHT_EYE,
                 {color: '#FF3030'});
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_RIGHT_EYEBROW,
                 {color: '#FF3030'});
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_LEFT_EYE,
                 {color: '#30FF30'});
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_LEFT_EYEBROW,
                 {color: '#30FF30'});
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_FACE_OVAL,
                 {color: '#E0E0E0'});
             drawingUtils.drawConnectors(
                 canvasCtx, landmarks, mpFaceMesh.FACEMESH_LIPS,
                 {color: '#E0E0E0'});

             let lm = face_feature_utils.landmarks_to_array(landmarks);
             let eye_nose_ratio = face_feature_utils.eye_nose_ratio(lm);
             let eye_nose_chin_ratio = face_feature_utils.eye_nose_chin_ratio(lm);
             let iris = face_feature_utils.iris_look_direction(lm);
             iris_buffer.enq(iris);

             if(eye_nose_ratio < -1.0) {
                 eye_nose_ratio = -1.0;
             }
             if(eye_nose_ratio > 1.0) {
                 eye_nose_ratio = 1.0;
             }
             let screen_fraction_x = 0.5 + eye_nose_ratio / -2.0;

             let screen_fraction_y = 0.5 + eye_nose_chin_ratio;
             if(screen_fraction_y < 0) {
                 screen_fraction_y = 0;
             }
             if(screen_fraction_y > 1.0) {
                 screen_fraction_y = 1.0;
             }

             // Add iris measurements with some hacky hacks.
             let iris_arr = iris_buffer.toarray();
             let iris_x = iris_arr.map(t => t[0]).reduce((sum, num) => sum + num, 0) / iris_arr.length;
             iris_x *= 3.0;
             let damp_x = 1.0 - Math.abs(2*eye_nose_ratio);
             if(damp_x < 0.3) {
                 damp_x = 0.0;
             }
             iris_x *= damp_x * damp_x;

             if(iris_x < -0.5) {
                 iris_x = -0.5;
             }
             if(iris_x > 0.5) {
                 iris_x = 0.5;
             }


             let iris_y = iris_arr.map(t => t[1]).reduce((sum, num) => sum + num, 0) / iris_arr.length;
             iris_y *= 5.0;
             let damp_y = 1.0 - Math.abs(2*eye_nose_chin_ratio);
             if(damp_y < 0.3) {
                 damp_y = 0.0;
             }
             iris_y *= damp_y * damp_y;

             if(iris_y < -0.5) {
                 iris_y = -0.5;
             }
             if(iris_y > 0.5) {
                 iris_y = 0.5;
             }

             if(use_iris_data) {
                 screen_fraction_x += iris_x;
                 screen_fraction_y += iris_y;
             }

             var is_blink = face_feature_utils.is_blinking(lm);

             // don't try to detect blinks if we are not looking into the center.
             let center_x = Math.abs(eye_nose_ratio);
             let center_y = Math.abs(eye_nose_chin_ratio);
             if(center_x < 0.8 && center_y < 0.4) {
                 blink_buffer.enq(is_blink);
             } else {
                 // console.log(`center_x: ${center_x}, center_y: ${center_y}`);
                 blink_buffer.enq(false);
             }

             var double_blink = false;
             if (detect_double_blink(blink_buffer.toarray())) {
                 // Trigger something based on double blink
                 console.log("double blink detected!!!");
                 blink_buffer = new CircularBuffer(blink_buffer_size);
                 double_blink = true;
             }


             // console.log(screen_fraction_x, screen_fraction_y)
             move_selector_box(screen_fraction_x, screen_fraction_y);
             let active = get_active_quad(screen_fraction_x, screen_fraction_y);
             quadrant_selector(active, double_blink);

            // canvasCtx.beginPath();
            // canvasCtx.arc(canvasElement.width * screen_fraction_x,
            //               canvasElement.height * screen_fraction_y,
            //               10.0, 0.0, 2.0*Math.PI, true);

            // canvasCtx.fill();

         }
     }
     canvasCtx.restore();
 }



var cameraOn = false;
const video  =  document.querySelector('.input_video');
video.muted = true;
const canvasElement = document.getElementsByClassName('output_canvas')[0];
canvasElement.width  = 1080;
canvasElement.height = 720;
const canvasCtx = canvasElement.getContext('2d');

const faceMesh = new mpFaceMesh.FaceMesh(config);
faceMesh.setOptions(solutionOptions);
faceMesh.onResults(onResults);


// for selecting input stream.
const select = document.getElementById('camera-select');
let currentStream;

function stopMediaTracks(stream) {
  stream.getTracks().forEach(track => {
    track.stop();
  });
}

 const iris_button = document.getElementById('iris-button');

 const button = document.getElementById('camera-button');

 button.addEventListener('click', async ()  => {

     cameraOn = !cameraOn;


       if (cameraOn) {
            unhide_selector_box()
           let constraints = {
                facingMode: 'user',
                width: {ideal:1920},
                height: {ideal:1080},
                // fps: 10
            };
           if (select.value === '') {
               console.log('Using any camera');
               constraints.facingMode = 'environment';
           } else {
               console.log(`Using camera: ${select.value}`);
               constraints.deviceId = { exact: select.value };
           }
           video.srcObject = await navigator.mediaDevices.getUserMedia({ video: constraints });
           currentStream = video.srcObject;
           video.play();
        //    video.addEventListener('loadedmetadata', () => {
        //        video.requestPictureInPicture()
        //            .catch(console.error);
        //    });

           const frame_cb = async (timestamp, frame) => {
               await faceMesh.send({image: video});
               video.requestVideoFrameCallback(frame_cb);
           };
           video.requestVideoFrameCallback(frame_cb);
       } else {
            reset_selector_box()
           utils.set_active_quadrant(0, false);

           if (typeof currentStream !== 'undefined') {
               stopMediaTracks(currentStream);
           }

           if (document.pictureInPictureElement) {
               document.exitPictureInPicture();
           }


       }

 });


var use_iris_data = false;
iris_button.addEventListener('click', async ()  => {
    use_iris_data = !use_iris_data;
    if(use_iris_data) {
        iris_button.style.color = "#ff3e50";
    } else {
        iris_button.style.color = "#2c3e50";
    }
});


// window.addEventListener("resize", function () {
//     overlayCanvasElement.width = window.innerWidth;
//     overlayCanvasElement.height = window.innerHeight;
// });
// window.addEventListener("load", function () {
//     overlayCanvasElement.width = window.innerWidth;
//     overlayCanvasElement.height = window.innerHeight;
// });


function gotDevices(mediaDevices) {
    select.innerHTML = '';
    let count = 1;
    let added = false;
    mediaDevices.forEach(mediaDevice => {
        if (mediaDevice.kind === 'videoinput') {
            const option = document.createElement('option');
            option.value = mediaDevice.deviceId;
            const label = mediaDevice.label || `Camera ${count++}`;
            const textNode = document.createTextNode(label);
            option.appendChild(textNode);
            select.appendChild(option);
            console.log(`${option.label} - ${option.value}`);
            added = true;
        }
    });
    if (!added) {
        select.appendChild(document.createElement('option'));
    }
}

navigator.mediaDevices.enumerateDevices().then(gotDevices);

let video_divs = ["test1", "test2", "test3", "test4"];
video_divs.forEach(div => {
    let element = document.getElementById(div);
    let video_element = element.querySelector("video");
    video_element.addEventListener("ended", async ()  => {
        if(entered_selection && `test${locked_quad}` == div) {
            // reset selection because video finished. (warning, hacky copy paste from quadrant_selector)
            console.log(`VIDEO ${div} FINISHED. RELEASING SELECTION.`);
            entered_selection = false;
            start_time_out = null;
            locked_quad = 0;
            unhide_selector_box();
            quadrant_buffer = new CircularBuffer(quadbuffer_size);
        }
        video_element.play();
        // console.log(`VIDEO ${div} FINISHED. RESTARTING.`);
    });
});
