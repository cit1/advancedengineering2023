import cv2
import numpy as np
from circular_buffer_numpy.circular_buffer import CircularBuffer
import mediapipe as mp
import face_feature_utils
import map_look_direction
import math
import time

# measure camera fps
prev_time = time.time()

# webcam input
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

with mp.solutions.face_mesh.FaceMesh(
        max_num_faces=1,
        refine_landmarks=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as face_mesh:
    while cap.isOpened():
        new_time = time.time()
        elapsed = new_time - prev_time
        prev_time = new_time
        fps = round(1 / elapsed)

        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = face_mesh.process(image)

        if results.multi_face_landmarks and len(results.multi_face_landmarks) == 1:
            lm = face_feature_utils.landmarks_to_array(results.multi_face_landmarks[0])

            lm_norm = (lm - np.mean(lm, axis=0) )/ (np.std(lm, axis=0))
            print("lm: ",np.max(lm), np.min(lm),np.mean(lm, axis=0) )
            print("lm norm: ",np.max(lm_norm), np.min(lm_norm),np.mean(lm, axis=0))

            # eye_nose_ratio = face_feature_utils.eye_nose_ratio(lm)
            # eye_nose_chin_ratio = face_feature_utils.eye_nose_chin_ratio(lm)
            # iris_look = face_feature_utils.iris_look_direction(lm)

            # iris_rounded = tuple([round(x,2) for x in iris_look])
            # print(f'fps: {fps}, eye nose ratio: {round(eye_nose_ratio, 2)}, eye nose chin ratio: {round(eye_nose_chin_ratio,2)}, iris: {iris_rounded}')



cap.release()
