import os, sys, json
import numpy as np
import pandas as pd
import sklearn
import joblib


from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import StackingClassifier, VotingClassifier
from sklearn.model_selection import KFold, GridSearchCV
from sklearn.metrics import accuracy_score, make_scorer
from sklearn.feature_selection import SelectKBest, chi2

# classifiers
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC,LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import SGDClassifier, LogisticRegression

from utils import file_exists, find_all_files_in_folder


saved_model_path = "./models/mlp_model_latest.joblib"

def train(x,y):
    
    

    # Create the train and test data
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)

    score_te_priliminary = 0.0
    if file_exists(saved_model_path) :
        model = joblib.load(saved_model_path)
        score_te_priliminary = model.score(X_test, y_test)
    else:
        model = MLPClassifier(hidden_layer_sizes=(200, 100), activation='tanh',max_iter=1000,verbose=True, learning_rate_init=0.0001)
        
        


    

    model.fit(X_train, y_train)

    # Predict class labels on a test data
    pred_labels_te = model.predict(X_test)


    print('*************** Evaluation on Test Data ***************')
    score_te = model.score(X_test, y_test)
    print('Accuracy Score: ', score_te)
    # Look at classification report to evaluate the model
    print(classification_report(y_test, pred_labels_te))
    print('--------------------------------------------------------')
    print("")
    print(f"prev acc: {score_te_priliminary}, current: {score_te}")
    if score_te >= score_te_priliminary:
        print(f"prev acc: {score_te_priliminary}, current: {score_te}")
        joblib.dump(model,saved_model_path)

    return model


def main():
    training_files = find_all_files_in_folder("./data", extension=".json")

    # training_files = [
    #     "./data/facemesh_data_sidkas_2023-02-17_1676643598.579158.json"
    # ]
    combined_df = pd.DataFrame()
    for training_data_path in training_files:
        data = json.load(open(training_data_path, 'r'))
        print(len(data))
        df = pd.DataFrame(data.values())
        combined_df = pd.concat((combined_df,df))
        
    x = np.array(combined_df.landmarks.values.tolist())
    y = np.array(combined_df.quadrant.values.tolist())
    print(np.shape(x),np.shape(y))

    print("Files: ",training_data_path)
    train(x=x, y=y)

class Inference():
    def __init__(self) -> None:
        if file_exists(saved_model_path):
            self.model = joblib.load(saved_model_path)
        else:
            raise Exception(f"model file does not exist {saved_model_path}")
        
    def predict(self, x):

        """
        x: flat ndarray
        """

        return self.model.predict_proba(x)



if __name__ == '__main__':
    main()