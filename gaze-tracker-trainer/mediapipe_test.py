import cv2
import numpy as np
from circular_buffer_numpy.circular_buffer import CircularBuffer
import mediapipe as mp
import face_feature_utils
import map_look_direction
import math
import time


def detect_double_blink(blink_buffer):
    blinks = 0
    is_blinking = False
    for b in blink_buffer:
        if b:
            if not is_blinking:
                blinks += 1
                is_blinking = True
                if blinks >= 2:
                    return True
        else:
            is_blinking = False
    return blinks >= 2


mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_face_mesh = mp.solutions.face_mesh
mp_face_features = mp.solutions.face_detection

blink_buffer_size = 20
blink_buffer = CircularBuffer(blink_buffer_size, dtype=bool)
is_blink = np.empty(1, dtype=bool)


active_quadrant_buffer_size = 5
active_quadrant_buffer = CircularBuffer(active_quadrant_buffer_size, dtype=int)
active_quadrant_buffer.reset(clear=True)
active_quadrant = np.empty(1, dtype=int)

iris_buffer_size = 4
iris_look_x = CircularBuffer(iris_buffer_size)
iris_look_y = CircularBuffer(iris_buffer_size)

# green dot buffer
green_dot_buffer_size = 5
green_dot_buffer = CircularBuffer(shape=(green_dot_buffer_size,2))
green_dot_buffer.reset(clear=True)


# measure camera fps
prev_time = time.time()

# show double blink for half a second
prev_double_blink_time = time.time()

# hide gui elements...
hide = False

# For webcam input:
drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1)
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

with mp_face_mesh.FaceMesh(
        max_num_faces=1,
        refine_landmarks=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as face_mesh:
    while cap.isOpened():
        new_time = time.time()
        elapsed = new_time - prev_time
        prev_time = new_time
        fps = round(1 / elapsed)


        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = face_mesh.process(image)

        # Draw the face mesh annotations on the image.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        look = None
        nose_look = None
        looking_at = None
        iris_look = None
        eye_nose_ratio = None
        eye_nose_chin_ratio = None
        screen_x = None
        screen_y = None
        if results.multi_face_landmarks:
            for face_landmarks in results.multi_face_landmarks:
                if not hide:
                    mp_drawing.draw_landmarks(
                        image=image,
                        landmark_list=face_landmarks,
                        connections=mp_face_mesh.FACEMESH_TESSELATION,
                        landmark_drawing_spec=None,
                        connection_drawing_spec=mp_drawing_styles
                        .get_default_face_mesh_tesselation_style())
                    mp_drawing.draw_landmarks(
                        image=image,
                        landmark_list=face_landmarks,
                        connections=mp_face_mesh.FACEMESH_CONTOURS,
                        landmark_drawing_spec=None,
                        connection_drawing_spec=mp_drawing_styles
                        .get_default_face_mesh_contours_style())
                    mp_drawing.draw_landmarks(
                        image=image,
                        landmark_list=face_landmarks,
                        connections=mp_face_mesh.FACEMESH_IRISES,
                        landmark_drawing_spec=None,
                        connection_drawing_spec=mp_drawing_styles
                        .get_default_face_mesh_iris_connections_style())

                # lm = np.array(face_landmarks)

                lm = face_feature_utils.landmarks_to_array(face_landmarks)
                look = face_feature_utils.face_look_vector(lm)
                nose_look = face_feature_utils.nose_look_vector(lm)

                iris_look = face_feature_utils.iris_look_direction(lm)

                looking_at = map_look_direction.face_vectors_to_viewpoint(look, nose_look)
                eye_nose_ratio = face_feature_utils.eye_nose_ratio(lm)
                eye_nose_chin_ratio = face_feature_utils.eye_nose_chin_ratio(lm)

                # if look[0] < 0:
                #     print("looking left")
                # if look[1] < 0:
                #     print("looking down")
                # if  0.5 < look[1] < 0.72:
                #     print("looking forward")
                # if look[1] > 0.72:
                #     print("looking up")

                is_blink[0] = face_feature_utils.is_blinking(lm)
                blink_buffer.append(is_blink)
                # if is_blink[0]:
                #     print("Both Eyes closed")
                if blink_buffer.buffer.any():
                    # print("Blink detected")
                    if detect_double_blink(blink_buffer.buffer):
                        print("Double blink detected")
                        prev_double_blink_time = new_time
                        # print("face", look)
                        # print("nose", nose_look)
                        blink_buffer.reset(clear=True)
                # print(f"right_eye area : {right_eye.area * 1000}")
        # Flip the image horizontally for a selfie-view display.
        image = cv2.flip(image, 1)
        im_height, im_width, _ = image.shape
        center = (im_width // 2, im_height // 2)

        line_len = 400
        if not hide:
            cv2.putText(image, f"fps: {fps}", (center[0] , center[1] - 400), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)
        # if look is not None:
        #     im_height, im_width, _ = image.shape
        #     center = (im_width // 2, im_height // 2)
        #     cv2.line(image, center, (center[0] + int(look[0] * line_len), center[1] + int(look[1] * line_len)), (255, 0,0), 2)
        # if nose_look is not None:
        #     im_height, im_width, _ = image.shape
        #     center = (im_width // 2, im_height // 2)
        #     cv2.line(image, center, (center[0] + int(nose_look[0] * line_len), center[1] + int(nose_look[1] * line_len)), (0, 255, 0), 2)
        #     cross_vec = np.cross(look, nose_look)
        #     cv2.line(image, center, (center[0] + int(cross_vec[0] * line_len), center[1] + int(cross_vec[1] * line_len)), (0, 0, 255), 2)
        # if iris_look is not None:
        #     im_height, im_width, _ = image.shape
        #     center = (im_width // 2, im_height // 2)
        #     cv2.putText(image, f"Iris: ({round(iris_look[0],4)},{round(iris_look[1],4)})", (center[0] , center[1] + 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)
        if eye_nose_ratio is not None:
            if not hide:
                cv2.putText(image, f"Nose Eye Ratio: {round(eye_nose_ratio, 4)}", (center[0] , center[1] + 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)

            if eye_nose_ratio < -0.5:
                message = "Looking left"
            elif eye_nose_ratio > 0.5:
                message = "Looking right"
            else:
                message = "Looking forward"
            if not hide:
                cv2.putText(image, message, (center[0] , center[1] + 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)
            if eye_nose_ratio < -1:
                eye_nose_ratio = -1
            if eye_nose_ratio > 1:
                eye_nose_ratio = 1
            screen_fraction = 0.5 + eye_nose_ratio / 2
            if not hide:
                cv2.circle(image, (int(im_width*screen_fraction),center[1]), 10, (0, 0, 255), -1)

            if eye_nose_chin_ratio is not None:
                if not hide:
                    cv2.putText(image, f"Nose Eye Chin Ratio: {round(eye_nose_chin_ratio, 4)}", (center[0] , center[1] - 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)
                screen_fraction_chin = 0.5 + eye_nose_chin_ratio
                if screen_fraction_chin < 0:
                    screen_fraction_chin = 0
                if screen_fraction_chin > 1.0:
                    screen_fraction_chin = 1.0

                if not hide:
                    cv2.circle(image, (int(im_width*screen_fraction),int(screen_fraction_chin*im_height)), 10, (255, 0, 255), -1)

                if iris_look is not None:
                    tx = np.empty(1)
                    tx[0] = (iris_look[0] + iris_look[1]) / 2.0
                    iris_look_x.append(tx)
                    iris_x = 2 * np.sum(iris_look_x.get_all(), axis=0) / iris_buffer_size
                    damp_x = 1.0 - abs(2*eye_nose_ratio)
                    if damp_x < 0.3:
                        damp_x = 0.0
                    iris_x *= damp_x * damp_x

                    if iris_x < -0.5:
                        iris_x = -0.5
                    if iris_x > 0.5:
                        iris_x = 0.5

                    ty = np.empty(1)
                    ty[0] = (iris_look[2] + iris_look[3]) / 2.0
                    iris_look_y.append(ty)
                    iris_y = -3.0*np.sum(iris_look_y.get_all(), axis=0) / iris_buffer_size
                    damp_y = 1.0 - abs(2*eye_nose_chin_ratio)
                    if damp_y < 0.3:
                        damp_y = 0.0
                    iris_y *= damp_y * damp_y
                    # print(f'Damp_x: {damp_x}, damp_y: {damp_y}')

                    if iris_y < -0.5:
                        iris_y = -0.5
                    if iris_y > 0.5:
                        iris_y = 0.5


                    screen_x = int(im_width * (screen_fraction + iris_x))
                    screen_y = int(im_height * (screen_fraction_chin + iris_y))

                    # buffer screen coordinates for green dot drawing
                    gb = np.empty(2, dtype=int)
                    gb[0] = screen_x
                    gb[1] = screen_y
                    green_dot_buffer.append(gb)

                    if not hide:
                        cv2.circle(image, (screen_x, screen_y), 10, (0, 255, 0), -1)
                        cv2.putText(image, f"Iris: ({round((iris_look[0] + iris_look[1]) / 2.0,4)},{round((iris_look[2] + iris_look[3]) / 2.0,4)})",
                                    (center[0] , center[1] - 50),
                                    # (screen_x, screen_y + 50)
                                    cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)



        # if look is not None:
        #     im_height, im_width,  _ = image.shape
        #     center = (im_height // 2, im_width // 2)
        #     look_prod = look[0] * look[1]
        #     cv2.putText(image, f"Look: {look}", center, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1, cv2.LINE_AA)
        # if nose_look is not None:
        #     im_height, im_width, _ = image.shape
        #     center = (im_height // 2, im_width // 2 + 50)
        #     cv2.putText(image, f"Nose: {nose_look}", center, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1,
        #                 cv2.LINE_AA)
        #     center = (im_height // 2, im_width // 2 + 100)
        #     cv2.putText(image, f"Cross: {np.cross(look,nose_look)}", center, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1)

        # if looking_at is not None:
        #
        #     im_height, im_width, _ = image.shape
        #     look_w = (looking_at[0]) * im_width
        #     look_h = (looking_at[1]) * im_height
        #     cv2.circle(image, (int(look_w), int(look_h)), 10, (0, 0, 255), -1)
        #     center = (im_height // 2, im_width // 2 + 100)
        #     cv2.putText(image, f"Looking at: {looking_at}", center, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1)


        if hide:
            image = np.zeros((im_height, im_width, 3), np.uint8)
            green_dot_mean = np.mean(green_dot_buffer.get_all(), axis=0)
            green_dot_var = np.var(green_dot_buffer.get_all(), axis=0)
            green_dot_var = (green_dot_var[0] + green_dot_var[1]) / 2.0
            green_dot_var *= 0.1
            if green_dot_var < 10:
                green_dot_var = 10
            if green_dot_var > 100:
                green_dot_var = 100
            print(f'x: {green_dot_var}')
            cv2.circle(image, (round(green_dot_mean[0]), round(green_dot_mean[1])), round(green_dot_var), (0, 255, 0), -1)

        overlay = image.copy()
        separator = 100
        alpha = 0.4
        active_quadrant[0] = 0
        if screen_x is not None and screen_y is not None:
            if screen_x < int(im_width / 2.0 - separator) and \
               screen_y < int(im_height / 2.0 - separator):
                active_quadrant[0] = 1
            if screen_x < int(im_width / 2.0 - separator) and \
               screen_y > int(im_height / 2.0 + separator):
                active_quadrant[0] = 2
            if screen_x > int(im_width / 2.0 + separator) and \
               screen_y < int(im_height / 2.0 - separator):
                active_quadrant[0] = 3
            if screen_x > int(im_width / 2.0 + separator) and \
               screen_y > int(im_height / 2.0 + separator):
                active_quadrant[0] = 4

            active_quadrant_buffer.append(active_quadrant)
            # print(f'{active_quadrant_buffer.get_all()}')
            active_quadrant_is = np.bincount(active_quadrant_buffer.get_all()).argmax()

            color = (0, 0, 200) if new_time - prev_double_blink_time < 0.5 else (0, 200, 0)
            if active_quadrant_is == 1:
                x, y, w, h = 0, 0, int(im_width / 2.0 - separator), int(im_height / 2.0 - separator)
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)
                image = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)
            if active_quadrant_is == 2:
                x, y, w, h = 0, int(im_height / 2.0 + separator), int(im_width / 2.0 - separator), im_height - 1
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)
                image = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)
            if active_quadrant_is == 3:
                x, y, w, h = int(im_width / 2.0 + separator), 0, im_width-1, int(im_height / 2.0 - separator)
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)
                image = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)
            if active_quadrant_is == 4:
                x, y, w, h = int(im_width / 2.0 + separator), int(im_height / 2.0 + separator), im_width - 1, im_height - 1
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)
                image = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)


        cv2.imshow('Advanced Engineering 2023', image)
        wk = cv2.waitKey(5)
        if wk & 0xFF == 32:
            hide = not hide
        if wk & 0xFF == 27:
            break
cap.release()
