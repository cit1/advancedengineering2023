import cv2
import numpy as np
from circular_buffer_numpy.circular_buffer import CircularBuffer
import mediapipe as mp
import face_feature_utils
import map_look_direction
import math
import time, sys, os, datetime
from utils import DictStorage, get_timestamp_ISO
from quadrant_classifier import Inference

# measure camera fps
prev_time = time.time()

# webcam input
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

buffer_length = 5
# Vars

hide = False
buffer = CircularBuffer(shape = (buffer_length,1434), dtype = 'float64')

## Contants


inference = Inference()

init_time = time.time()
active_quadrant = -1

windowed_data = np.ndarray(shape=(10, 443))

buffer_count = 0 

with mp.solutions.face_mesh.FaceMesh(
        max_num_faces=1,
        refine_landmarks=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as face_mesh:
    while cap.isOpened():
        new_time = time.time()
        elapsed = new_time - prev_time
        prev_time = new_time
        fps = round(1 / elapsed)

        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = face_mesh.process(image)

        # Flip the image horizontally for a selfie-view display.
        image = cv2.flip(image, 1)
        im_height, im_width, _ = image.shape
        center = (im_width // 2, im_height // 2)

        if results.multi_face_landmarks and len(results.multi_face_landmarks) == 1:
            lm = face_feature_utils.landmarks_to_array(results.multi_face_landmarks[0])


            buffer.append(lm.flatten())
            if buffer_count> buffer_length :
                quad_prob = inference.predict(buffer.get_data())
            
                meadin_probs = np.median(quad_prob,axis=0)
                active_quadrant = np.argmax(meadin_probs)

                # is_blink = face_feature_utils.is_blinking(lm)
            else:
                buffer_count +=1

            if hide:
                image = np.zeros((im_height, im_width, 3), np.uint8)
            overlay =  image.copy()
            separator = 0
            alpha = 0.9

            color = (0, 200, 0)

            w, h = int(im_width / 2.0 - separator), int(im_height / 2.0 - separator)
            if active_quadrant == 1:
                x, y, = 0, 0 
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 2:
                x, y = 0, int(im_height / 2.0 + separator),
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 3:
                x, y = int(im_width / 2.0 + separator), 0
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 4:
                x, y = int(im_width / 2.0 + separator), int(im_height / 2.0 + separator)
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 0:
                x,y = center
                w,h = 0,0
                cv2.circle(overlay, center=center, radius=200, color=color, thickness=-1)


            current_time = time.time()
            disp_timer = int(current_time - init_time)

            if active_quadrant >= 0:
                quadrant_center = (int(x + w/2), int(y + h/2))
                font = cv2.FONT_HERSHEY_SIMPLEX
                text = f"Selected {active_quadrant}!"
                thickness = 6
                fontscale = 1.5

                # get boundary of this text
                textsize = cv2.getTextSize(text, font,fontscale, thickness=thickness)[0]

                # get coords based on boundary
                textX = int(quadrant_center[0] - (textsize[0] / 2))
                textY = int(quadrant_center[1] + (textsize[1] / 2))


                
                cv2.putText(overlay, text, (textX, textY), font,fontscale, (255,255,255), thickness=thickness)

            image = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)

            
        

        cv2.imshow('Advanced Engineering 2023', cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
        wk = cv2.waitKey(5)
        if wk & 0xFF == 32:
            hide = not hide
        if wk & 0xFF == 27:
            break

cap.release()
