import numpy as np
import mediapipe as mp
from math import log

mp_face_mesh = mp.solutions.face_mesh

# From https://github.com/google/mediapipe/blob/a908d668c730da128dfa8d9f6bd25d519d006692/mediapipe/modules/face_geometry/data/canonical_face_model_uv_visualization.png
RIGHT_EYE_VERTS = [33, 7, 163, 144, 145, 153, 154, 155, 133] + [173, 157, 158, 159, 160, 161, 246]
RIGHT_EYE_VERTS.append(RIGHT_EYE_VERTS[0])

RIGHT_EYE_CORNERS = [33,133]
# RIGHT_EYE_TOP_BOTTOM = [ 159, 145 ]
RIGHT_EYE_TOP_BOTTOM = [ 27, 23 ]

LEFT_EYE_VERTS = [263, 249, 390, 373, 374, 380, 381, 382, 362] + [398, 384, 385, 387, 388, 466]
LEFT_EYE_VERTS.append(LEFT_EYE_VERTS[0])

LEFT_EYE_CORNERS = [263,362]
# LEFT_EYE_TOP_BOTTOM = [ 386, 374 ]
LEFT_EYE_TOP_BOTTOM = [ 257, 253 ]
RIGHT_IRIS_VERTS = list({s[0] for s in mp_face_mesh.FACEMESH_RIGHT_IRIS})
LEFT_IRIS_VERTS = list({s[0] for s in mp_face_mesh.FACEMESH_LEFT_IRIS})

FACE_OUTLINE_VERTS = list({s[0] for s in mp_face_mesh.FACEMESH_FACE_OVAL})

NOSE_PROFILE_VERTS = [2, 194, 19, 1, 4, 5, 195] #+ [141,125,44,45,51] + [270,354,274,275,261]
NOSE_TIP = 1

CHIN = 152

NOSE_EYE_RATIO_OFFSET = 0.1
NOSE_EYE_CHIN_RATIO_OFFSET = -0.15
IRIS_EYE_RATIO_OFFSET = -1.0

# EYE_CLOSED_VALUE = 0.00018
EYE_CLOSED_VALUE = 0.00018


def landmarks_to_array(face_landmarks):
    lm = np.array([[lm.x, lm.y, lm.z] for lm in face_landmarks.landmark])
    return lm


def face_look_vector(lm):
    #u, s, vh = np.linalg.svd(lm[FACE_OUTLINE_VERTS])
    u, s, vh = np.linalg.svd(lm)
    #look = np.cross(vh[0],vh[1])
    look = vh[2]
    look = look / np.linalg.norm(look)
    return look

def nose_look_vector(lm):
    nose_profile = lm[NOSE_PROFILE_VERTS]
    u_nose, s_nose, vh_nose = np.linalg.svd(nose_profile)
    # nose_look = np.cross(vh_nose[0], vh_nose[1])
    nose_look = vh_nose[2]
    nose_look = nose_look / np.linalg.norm(nose_look)
    return nose_look

def iris_look_direction(lm):
    left_iris = np.sum(lm[LEFT_IRIS_VERTS], axis=0) / len(LEFT_IRIS_VERTS)
    right_iris = np.sum(lm[RIGHT_IRIS_VERTS], axis=0) / len(RIGHT_IRIS_VERTS)

    h_left_iris_left_offset = np.linalg.norm(lm[LEFT_EYE_CORNERS[0]][0] - left_iris[0])
    h_left_iris_right_offset = np.linalg.norm(lm[LEFT_EYE_CORNERS[1]][0] - left_iris[0])
    h_right_iris_left_offset = np.linalg.norm(lm[RIGHT_EYE_CORNERS[0]][0] - right_iris[0])
    h_right_iris_right_offset = np.linalg.norm(lm[RIGHT_EYE_CORNERS[1]][0] - right_iris[0])
    h_left_ratio = log(h_left_iris_left_offset / h_left_iris_right_offset)
    h_right_ratio = log(h_right_iris_right_offset / h_right_iris_left_offset)

    v_left_eye_bottom = lm[LEFT_EYE_TOP_BOTTOM[0]][1]
    v_left_eye_top = lm[LEFT_EYE_TOP_BOTTOM[1]][1]
    v_left_iris = left_iris[1]

    v_right_eye_bottom = lm[RIGHT_EYE_TOP_BOTTOM[0]][1]
    v_right_eye_top = lm[RIGHT_EYE_TOP_BOTTOM[1]][1]
    v_right_iris = right_iris[1]

    v_left_ratio = 0 if v_left_iris > v_left_eye_top or v_left_iris < v_left_eye_bottom else \
        (abs(1.52*(v_left_eye_top - v_left_iris)) / abs((v_left_iris - v_left_eye_bottom))) + IRIS_EYE_RATIO_OFFSET
    v_right_ratio = 0 if v_right_iris > v_right_eye_top or v_right_iris < v_right_eye_bottom else \
        (abs(1.52*(v_right_eye_top - v_right_iris)) / abs((v_right_iris - v_right_eye_bottom))) + IRIS_EYE_RATIO_OFFSET

    mid = (v_left_ratio + v_right_ratio) / 2.0
    # print(f'ivl: {v_left_ratio} ivr: {v_right_ratio}, mid: {mid}')

    return h_left_ratio, h_right_ratio, v_left_ratio, v_right_ratio

def eye_nose_ratio(lm):
    left_eye_outer_x = lm[LEFT_EYE_CORNERS[0]][0]
    left_eye_inner_x = lm[LEFT_EYE_CORNERS[1]][0]
    right_eye_outer_x = lm[RIGHT_EYE_CORNERS[0]][0]
    right_eye_inner_x = lm[RIGHT_EYE_CORNERS[1]][0]
    nose_x = lm[NOSE_TIP][0]
    return log((abs(nose_x - left_eye_outer_x)) / abs((nose_x -right_eye_outer_x)))+NOSE_EYE_RATIO_OFFSET

def eye_nose_chin_ratio(lm):
    left_eye_outer_y = lm[LEFT_EYE_CORNERS[0]][1]
    right_eye_outer_y = lm[RIGHT_EYE_CORNERS[0]][1]
    eye_y = (left_eye_outer_y + right_eye_outer_y) / 2.0
    chin_y = lm[CHIN][1]
    nose_y = lm[NOSE_TIP][1]
    # distance nose to eye is about half of nose to chin, scale accordingly
    return log((abs(nose_y - eye_y)*2.2) / abs((nose_y - chin_y))) + NOSE_EYE_CHIN_RATIO_OFFSET



def _poly_area(x, y):
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))

def is_blinking(lm):
    left_eye_area = _poly_area(lm[LEFT_EYE_VERTS, 0], lm[LEFT_EYE_VERTS, 1])
    right_eye_area = _poly_area(lm[RIGHT_EYE_VERTS, 0], lm[RIGHT_EYE_VERTS, 1])
    return left_eye_area < EYE_CLOSED_VALUE or right_eye_area < EYE_CLOSED_VALUE
