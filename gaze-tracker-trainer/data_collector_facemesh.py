import cv2
import numpy as np
from circular_buffer_numpy.circular_buffer import CircularBuffer
import mediapipe as mp
import face_feature_utils
import map_look_direction
import math
import time, sys, os, datetime
from utils import DictStorage, get_timestamp_ISO

# measure camera fps
prev_time = time.time()

# webcam input
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_face_mesh = mp.solutions.face_mesh
mp_face_features = mp.solutions.face_detection

# Vars
data_counter = 0
ignore_initial_datapoints_counter = 0
hide = False

## Contants
max_ignore_initial_datapoints_count = 30
max_data_points = 200
init_time_limit = 10

ts = str(time.time())
user = os.environ.get( "USERNAME")
if user is None:
    user = os.environ.get( "USER")
date = datetime.datetime.now().date().isoformat()
storage = DictStorage(file_name=f"./data/facemesh_data_{user}_{date}_{ts}.json")




def rotation(image, angleInDegrees):
    h, w = image.shape[:2]
    img_c = (w / 2, h / 2)

    rot = cv2.getRotationMatrix2D(img_c, angleInDegrees, 1)

    rad = math.radians(angleInDegrees)
    sin = math.sin(rad)
    cos = math.cos(rad)
    b_w = int((h * abs(sin)) + (w * abs(cos)))
    b_h = int((h * abs(cos)) + (w * abs(sin)))

    rot[0, 2] += ((b_w / 2) - img_c[0])
    rot[1, 2] += ((b_h / 2) - img_c[1])

    outImg = cv2.warpAffine(image, rot, (b_w, b_h), flags=cv2.INTER_LINEAR)
    return outImg


def overlay_image_alpha(img, img_overlay, x, y, alpha_mask):
    """Overlay `img_overlay` onto `img` at (x, y) and blend using `alpha_mask`.

    `alpha_mask` must have same HxW as `img_overlay` and values in range [0, 1].
    """
    # Image ranges
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    # Overlay ranges
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    # Exit if nothing to do
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    # Blend overlay within the determined ranges
    img_crop = img[y1:y2, x1:x2]
    img_overlay_crop = img_overlay[y1o:y2o, x1o:x2o]
    alpha = alpha_mask[y1o:y2o, x1o:x2o, np.newaxis]
    alpha_inv = 1.0 - alpha

    img_crop[:] = alpha * img_overlay_crop + alpha_inv * img_crop

def data_writer(landmarks,look,nose_look,iris_look,looking_at, eye_nose_ratio, eye_nose_chin_ratio, is_blink, quadrant):
    global data_counter , ignore_initial_datapoints_counter



    if ignore_initial_datapoints_counter > max_ignore_initial_datapoints_count:
        data = {"landmarks":landmarks.flatten().tolist(), "look":look.tolist(), "nose_look":nose_look.tolist(), "iris_look":iris_look,"looking_at":looking_at,"eye_nose_ratio":eye_nose_ratio ,"eye_nose_chin_ratio":eye_nose_chin_ratio, "is_blink":int(is_blink), "quadrant":quadrant }

        storage.update(key=data_counter,value=data)
        data_counter += 1

        if data_counter%max_data_points==0:
            ignore_initial_datapoints_counter = 0
    else:
        ignore_initial_datapoints_counter += 1



init_time = time.time()
active_quadrant = -1

with mp.solutions.face_mesh.FaceMesh(
        max_num_faces=1,
        refine_landmarks=True,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as face_mesh:
    while cap.isOpened():
        new_time = time.time()
        elapsed = new_time - prev_time
        prev_time = new_time
        fps = round(1 / elapsed)

        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = face_mesh.process(image)


        im_height, im_width, _ = image.shape
        center = (im_width // 2, im_height // 2)

        if results.multi_face_landmarks and len(results.multi_face_landmarks) == 1:
            if not hide:
                mp_drawing.draw_landmarks(
                    image=image,
                    landmark_list=results.multi_face_landmarks[0],
                    connections=mp_face_mesh.FACEMESH_CONTOURS,
                    landmark_drawing_spec=None,
                    connection_drawing_spec=mp_drawing_styles
                    .get_default_face_mesh_contours_style())

            # Flip the image horizontally for a selfie-view display.
            image = cv2.flip(image, 1)

            lm = face_feature_utils.landmarks_to_array(results.multi_face_landmarks[0])

            lm_norm = (lm - np.min(lm, axis=0) )/ (np.std(lm, axis=0))
            # print("lm: ",max(lm), min(lm))
            # print("lm norm: ",max(lm_norm), min(lm_norm))

            look = face_feature_utils.face_look_vector(lm)
            nose_look = face_feature_utils.nose_look_vector(lm)

            iris_look = face_feature_utils.iris_look_direction(lm)

            looking_at = map_look_direction.face_vectors_to_viewpoint(look, nose_look)
            eye_nose_ratio = face_feature_utils.eye_nose_ratio(lm)
            eye_nose_chin_ratio = face_feature_utils.eye_nose_chin_ratio(lm)

            is_blink = face_feature_utils.is_blinking(lm)

            if hide:
                image = np.zeros((im_height, im_width, 3), np.uint8)
            overlay =  image.copy()
            separator = 0
            alpha = 0.9
            color = (0, 200, 0)

            w, h = int(im_width / 2.0 - separator), int(im_height / 2.0 - separator)
            if active_quadrant == 1:
                x, y, = 0, 0
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 2:
                x, y = 0, int(im_height / 2.0 + separator),
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 3:
                x, y = int(im_width / 2.0 + separator), 0
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 4:
                x, y = int(im_width / 2.0 + separator), int(im_height / 2.0 + separator)
                cv2.rectangle(overlay, (x, y), (x+w, y+h), color, -1)

            if active_quadrant == 0:
                x,y = center
                w,h = 0,0
                cv2.circle(overlay, center=center, radius=200, color=color, thickness=-1)


            # draw something funky to keep attention...
            if active_quadrant > 0:
                small_width = int(image.shape[1] / (5 + 2.5*math.cos(50*data_counter)))
                small_height = int(image.shape[0] / (5 + 2.5*math.sin(50*data_counter)))
                dim = (small_width, small_height)
                small_img = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
                rotated_img = rotation(small_img, (5*data_counter) % 360);
                # make alpha channel
                tmp = cv2.cvtColor(rotated_img, cv2.COLOR_BGR2GRAY)
                (_, alpha_mask) = cv2.threshold(tmp,0,255,cv2.THRESH_BINARY)
                r_height, r_width, _ = rotated_img.shape
                pos = {
                    1: (50, 50),
                    2: (50, im_height - 50 - r_height),
                    3: (im_width - 50 - r_width, 50),
                    4: (im_width - 50 - r_width, im_height - 50 - r_height)
                }
                (xx, yy) = pos[active_quadrant]
                overlay_image_alpha(overlay, rotated_img, xx, yy, alpha_mask / 255.0)

            current_time = time.time()
            disp_timer = int(current_time - init_time)

            if active_quadrant >= 0:
                quadrant_center = (int(x + w/2), int(y + h/2))
                font = cv2.FONT_HERSHEY_SIMPLEX
                text = f"Look Here! {disp_timer}"
                thickness = 6
                fontscale = 1.5

                # get boundary of this text
                textsize = cv2.getTextSize(text, font,fontscale, thickness=thickness)[0]

                # get coords based on boundary
                textX = int(quadrant_center[0] - (textsize[0] / 2))
                textY = int(quadrant_center[1] + (textsize[1] / 2))



                cv2.putText(overlay, text, (textX, textY), font,fontscale, (255,255,255), thickness=thickness)
                data_writer(landmarks=lm,look=look,nose_look=nose_look,iris_look=iris_look,looking_at=looking_at, eye_nose_ratio=eye_nose_ratio, eye_nose_chin_ratio=eye_nose_chin_ratio, is_blink=is_blink, quadrant=active_quadrant)

            image = cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0)



            if disp_timer < init_time_limit:
                x,y = center
                cv2.putText(image, str(disp_timer), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 5, (255,255,255), 8)

            elif (data_counter >= 0 and data_counter <= 1 * max_data_points) :
                active_quadrant = 1

            elif (data_counter > 1 * max_data_points and data_counter <= 2 * max_data_points):
                active_quadrant = 2

            elif (data_counter > 2 * max_data_points and data_counter <= 3 * max_data_points):
                active_quadrant = 3

            elif (data_counter > 3 * max_data_points and data_counter <= 4 * max_data_points):
                active_quadrant = 4

            elif (data_counter > 4 * max_data_points and data_counter <= 5 * max_data_points):
                active_quadrant = 0

            elif (data_counter > 5 * max_data_points and data_counter <=  6 * max_data_points):
                active_quadrant = -1
                cv2.putText(image, "End of data sampling!", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12), 2)
                storage.save()
                print(f"data saved to {storage.file_name}")
                break

            elif data_counter > 6 * max_data_points  :
                break




            cv2.imshow('Advanced Engineering 2023', cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
            wk = cv2.waitKey(5)
            if wk & 0xFF == 32:
                hide = not hide
            if wk & 0xFF == 27:
                break

cap.release()
