# AdvancedEngineering2023


## Install: 
- https://google.github.io/mediapipe/getting_started/python.html

- linux:
    - python3 -m venv ae2023 && source ae2023/bin/activate
    - pip install  -r requirements.txt

- mac:
    - python3 -m venv ae2023 && source ae2023/bin/activate
    - pip install  -r requirements.txt
    - (on Mac) pip install mediapipe-silicon 

- windows:
    - python3 -m venv ae2023
    - ae2023\Scripts\activate
    - pip install  -r requirements.txt


## Collect data:

- Run python3 data_collector_facemesh.py
- Recording starts after 15 seconds 
- maximize the window in the 15 seconds time frame
- Try to look naturally at the highlighted quadrants
- Data is saved as json file in data folder
- Upload the file to sharepoint [AdvancedEnginnering data folder](https://chalmersindustriteknik.sharepoint.com/:f:/r/sites/AppliedAI/Delade%20dokument/General/AdvancedEngineering2023/Data?csf=1&web=1&e=cfhqdv)



