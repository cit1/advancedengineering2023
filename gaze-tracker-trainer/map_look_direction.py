import numpy as np

NOSE_X_MIDDLE = 0.25
NOSE_X_LEFT = 0.17
NOSE_X_RIGHT = 0.34
NOSE_X_UP = 0.11
NOSE_X_DOWN = 0.37

NOSE_Y_MIDDLE = -0.17
NOSE_Y_LEFT = -0.12
NOSE_Y_RIGHT = -0.22
NOSE_Y_UP = -0.05
NOSE_Y_DOWN = -0.27

NOSE_Z_MIDDLE = 0.27
NOSE_Z_LEFT = 0.99
NOSE_Z_RIGHT = 0.87
NOSE_Z_UP = 0.11
NOSE_Z_DOWN = 0.5

FACE_X_MIDDLE = -0.69
FACE_X_UP = -0.72
FACE_X_DOWN = -0.67

FACE_Y_MIDDLE = -0.70
FACE_Y_UP = -0.72
FACE_Y_DOWN = -0.74



def nose_vector_to_viewpoint(nose_vector):
    x,y,z = nose_vector
    lr_x_look = np.interp(x, [NOSE_X_LEFT, NOSE_X_MIDDLE, NOSE_X_RIGHT], [0, 0.5, 1])
    lr_y_look = np.interp(y, [NOSE_Y_LEFT, NOSE_Y_MIDDLE, NOSE_Y_RIGHT], [0, 0.5, 1])
    lr_z_look = np.interp(z, [NOSE_Z_LEFT, NOSE_Z_MIDDLE, NOSE_Z_RIGHT], [0, 0.5, 1])
    ud_x_look = np.interp(x, [NOSE_X_UP, NOSE_X_MIDDLE, NOSE_X_DOWN], [0, 0.5, 1])
    ud_y_look = np.interp(y, [NOSE_Y_DOWN, NOSE_Y_MIDDLE, NOSE_Y_UP], [1, 0.5, 0])
    ud_z_look = np.interp(z, [NOSE_Z_UP, NOSE_Z_MIDDLE, NOSE_Z_DOWN], [0, 0.5, 1])
    # lr_look = (lr_x_look + lr_y_look + lr_z_look) / 3
    # ud_look = (ud_x_look + ud_y_look + ud_z_look) / 3
    lr_look = 0.5 # lr_x_look
    ud_look = ud_z_look
    return lr_look, ud_look

def face_vector_to_viewpoint(face_vector):
    x,y,z = face_vector
    ud_x_look = np.interp(x, [FACE_X_DOWN, FACE_X_MIDDLE, FACE_X_UP], [1, 0.5, 0])
    ud_y_look = np.interp(y, [FACE_Y_DOWN, FACE_Y_MIDDLE, FACE_Y_UP], [1, 0.5, 0])
    ud_look = (ud_x_look + ud_y_look) / 2
    return 0.5, ud_look

def face_vectors_to_viewpoint(face_vector, nose_vector):
    lr_nose, ud_nose = nose_vector_to_viewpoint(nose_vector)
    lr_face, ud_face = face_vector_to_viewpoint(face_vector)
    return lr_nose,ud_nose
