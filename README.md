# gaze-tracker-ui


## Tasks:
- [x] Update data collection UI: make the square more enternaining
- [x] capture eye movements (we may have this already?)
- [x] port the face utils to js
- [x] Animation for quadrant expansion
- [x] play videos / video player plugin
- [x] some delay before and after the transition/animation (staying at one video for 30 seconds before getting back for example)

- [x] facemesh grid plot is not working, I tried to fix it but failed

- [x] User cue: moving circles/rectangles in reference to where the head is, similar to the one we had in python
- [ ] cleanup main.js
### classifier model
- [ ] Normalize the face landmark data for training
- [ ] Collect more data on the same setup
- [ ] Run classifier model in js if needed / Run model in js/create python binding/API
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
